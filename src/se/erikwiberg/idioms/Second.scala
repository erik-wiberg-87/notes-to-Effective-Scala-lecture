package se.erikwiberg.idioms

import java.io.PrintWriter
import scala.util.Try
import scala.BigInt
import scala.reflect.{ClassTag, classTag}
import scala.util.Random


object Second {
  def main(args: Array[String]): Unit = {
    
    withPrintWriter("hello.txt") {
      printWriter => 
        println("printing with passed in printWriter")
        printWriter println "First line!"
        printWriter println "Second line!"
        
        factorialListUpToLimit(300) map (printWriter println)
    }
    
    printTypeInfoOfListT( List(BigInt(100022222)) )
    
  }

  @scala.annotation.tailrec
  def factorialListUpToLimit(limit: Int)(implicit current: Int = 2, soFar: List[BigInt] = List(BigInt(1))): List[BigInt] = {
    if (current > limit) { // last iteration
      soFar.reverse
    } else {
      factorialListUpToLimit(limit)(current + 1, BigInt(current) * soFar.head :: soFar)
    }
  }
  
  
  /**
   * Type Parameters
   * 		TypeTag ClassTag
   * Gets around type erasure
   * You can get the compiler to send in the known type information
   * so that it's available at runtime.. 
   */
  
  def printTypeInfoOfListT[T : ClassTag](list: List[T]): Unit = {
    // classTag is imported from scala.reflect 
    val className = classTag[T].runtimeClass.getSimpleName
    println("Type T is: " + className)
  }
  
  
  /**
   * Generic Pattern-matching
   * example @ https://youtu.be/rhXUlPmLLts?t=9207
   */
  
  
  
  
  /**
   * "Loan" - kind of like Java 7 try with resources
   */
  
  def withPrintWriter(fileName: String)(fn: PrintWriter => Unit): Unit = {
    println("Creating printWriter")
    val printWriter = new PrintWriter(fileName)
    try {
      println("passing pw to the function that will use it")
      fn(printWriter)
    } finally {
      println("Closing printwriter")
      Try(printWriter.close())
    }
  }
  
  
  /*
*  Why NOT (always) Monads? https://youtu.be/rhXUlPmLLts?t=7642
*  Overkill
*  Monads don't mix
*  		Eg. Lists and Futures don't mix into a for(x <- foo)  expression
*  		(Possible to work around with transformers)
*  (Lack of) Ease of Use
*  		May be hard to debug, reason about
*  Pragmatism 
*  		You ain't gonna need it
*/

  
  
}