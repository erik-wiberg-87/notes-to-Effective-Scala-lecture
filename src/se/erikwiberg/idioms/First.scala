package se.erikwiberg.idioms

import scala.util.Random
import scala.util.Try

object First {
  def main(args: Array[String]): Unit = {
    val person1 = Person("Joan", "Johansson", 26, Some(Address.default))
    val person2 = Person("Steve", "Roberts", 32, None)
    println(person1)
    
    val person3 = maybePerson orElse Some( Person.default )
    person3 map println
    
    println(
        Option(nullableString) map { str => str.toUpperCase() } 
    )
    
    // dealing with nested Option
    val maybeAddressName1 = 
      for(person <- maybePerson; address <- person.address) yield address.streetName
    maybeAddressName1 map { addr => println("address 1 is: " + addr )}
    
    val maybeAddressName2 = 
      maybePerson flatMap { p => p.address } map {addr => addr.streetName }
    maybeAddressName2 map { addr => println("address 2 is: " + addr)}
    
    // alternative syntax
    val maybeAddressName3 = 
      maybePerson flatMap (_.address)  map (_.streetName)
    
    maybeAddressName3 map printWithStringFormat("Found address '%s' in Person")
    
    
    val integers = List(1,2,3,4)
    val squaredInts  = for(int <- integers) yield (int * int) 
    squaredInts foreach println
    for(int <- integers) println(int * 2)
    
    val division = Try {
      // can lead to java.lang.ArithmeticException: / by zero
      5 / (if(randCondition) 5 else 0)
    }
    
    println(division)
    println(division getOrElse -666)
    division orElse Try(Int.MinValue) map printWithStringFormat("Division resulted in: %s")
    
    println(factorialListFrom1toLimit(20))
    
  }
  
  def maybePerson = {
    if (randCondition) 
      Some(Person("Erik","Wiberg",29, Some(Address.default)))
    else None
  }
  
  def nullableString = if (randCondition) "A value!" else null
  
  def randCondition = Random nextBoolean
  
  /**
   * Takes a string format like "Hello, %s!" and returns a function that takes
   * one argument: Any and prints it (as a string) with the format.
   * Eg. printWithStringFormat("Hello, %s!")("World") prints "Hello World!"
   */
  def printWithStringFormat(format: String): Any => Unit = {
    formatArg => println(String.format(format, formatArg.toString()))
  }
  
  
  @scala.annotation.tailrec
  def factorialListFrom1toLimit(limit: Int)
    (implicit current: Int = 2, soFar: List[BigInt] = List(BigInt(1))): List[BigInt] = {
      if(current > limit) {
        soFar.reverse // last iteration. return the list, reversed
      } else {
        // adding to tail is expensive. prefer adding to tail, then do reverse ONCE at the end
        factorialListFrom1toLimit(limit)(current + 1, BigInt(current) * soFar.head :: soFar)
      }
    }
  
}

case class Address(streetName: String, streetNumber: Int)
object Address {
  def default: Address = Address("Cool street name", 22)
}

case class Person(firstName: String, lastName: String, age: Int, address: Option[Address])
object Person {
  def default: Person = Person("Default", "Default", 102, None) 
}